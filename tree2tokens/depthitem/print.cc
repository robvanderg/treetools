#include "depthitem.ih"

void DepthItem::print()
{
    int tabs = d_depth;
    while(tabs-- > 0){
        cout << "  ";
    }
    cout << d_content << '\n';
}
