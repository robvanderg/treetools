#ifndef INCLUDED_DEPTHITEM_
#define INCLUDED_DEPTHITEM_

#include <string>

class DepthItem
{
    public:
        int d_depth;
        std::string d_content;

        DepthItem();
        DepthItem(int depth, std::string content);
        void print();
    private:
};
        
#endif
