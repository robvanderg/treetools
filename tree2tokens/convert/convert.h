#ifndef INCLUDED_CONVERT_
#define INCLUDED_CONVERT_

#include "../depthitem/depthitem.h"

#include <vector>
#include <string>

class Convert
{
    std::vector<std::vector<DepthItem>> d_trees;

    public:
        Convert();

        void readBracket(std::string bracketPath);
        void writeTokens(size_t treeNum);

        size_t numTrees();
        void print();

    private:
        void replaceAll(std::string& subject, const std::string&search, const std::string& replace);
};
        
#endif
