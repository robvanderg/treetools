#include "convert.ih"

void Convert::readBracket(string parsePath)
{
    vector<DepthItem> depthList;
    int depth = 0;
    string tag = "";

    ifstream is(parsePath);
    char c;
    while (is.get(c)){
        switch (c){
            case '(':
                ++depth;
                break;
            case ')':
                if(tag.length() > 0){
                    depthList.push_back(DepthItem(++depth -1, tag));
                    tag = "";
                    --depth;
                }
                --depth;
                if(depth == 0){
                    d_trees.push_back(depthList);
                    depthList.clear(); 
                    tag="";
                }
                break;
            case ' ':
                if(tag.length() > 0){
                    depthList.push_back(DepthItem(depth -1, tag));
                    tag = "";
                }
                break;
            case '\n':
                break;
            default:
                tag += c;
                break;
        }
    }
}

