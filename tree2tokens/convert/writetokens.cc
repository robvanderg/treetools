#include "convert.ih"

void Convert::writeTokens(size_t treeNum)
{
    for (size_t nodeIdx = 0; nodeIdx != d_trees[treeNum].size(); ++nodeIdx)
        if (d_trees[treeNum][nodeIdx].d_depth + 1 != d_trees[treeNum][nodeIdx+1].d_depth)
            if (d_trees[treeNum][nodeIdx-1].d_content != "-NONE-")
                cout << d_trees[treeNum][nodeIdx].d_content << ' ';
    cout << '\n';
}

