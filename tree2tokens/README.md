# README #

A simple c++ application to extract tokens from PTB style trees

INPUT:
```
#!bash
(ROOT (SBARQ (WHNP (WP What) (DT a) (JJ nice) (NN application)) (. !)))
```
OUTPUT:
```
#!bash
What a nice application ! 
\end{tikzpicture}
```


### Installation ###

Simply run icmbuild from this projects directory

```
#!bash
cd penn2tokens
icmbuild
```

### Use ###


```
#!bash
./tmp/bin/binary input
```