#include "convert/convert.h"

#include <iostream>
#include <string>
#include <fstream>

int main(int argc, char *argv[])
{
    if (argc < 2)
        std::cout << "where is my argument?\n";
    Convert conv;
    conv.readBracket(argv[1]);
    for (size_t treeNum = 0; treeNum != conv.numTrees(); ++treeNum)
        conv.writeTokens(treeNum);

}
