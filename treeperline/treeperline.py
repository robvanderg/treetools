import sys

if len(sys.argv) < 2:
    print("where is my input? <treefile>")
    exit()

for line in open(sys.argv[1], 'r'):
    if line[0] == '(':
        print('\n' + line.strip() + ' ', end='')
    else:
        print(line.strip() + ' ', end='')
