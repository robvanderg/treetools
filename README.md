# Treetools #

This repository contains a collection of tools to convert ptb style constituency trees to different formats

### Contents: ###

* conll2text: convert conll dependency format to raw text
* tokensintree: replace the tokens in the tree by ne tags
* tree2conll: convert ptb trees to conll format with gold pos-tags
* tree2latex: convert ptb trees to latex format (tikz-qtree)
* tree2tokens: extract tokens from ptb trees (ignore epsilons)
* treebankComp: compare treebanks
* treecomp: compare the output of two different parsers against a gold standard treebank
* treeperline: convert trees that spawn multiple lines (wsj) to one line

Look in the folders of these applications for a more detailed readme. 

If you have any questions, contact r.van.der.goot@rug.nl
