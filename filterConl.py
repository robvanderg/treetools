import sys


def isInt(s):
    try:
        int(s)
        return True
    except ValueError:
        return False
if len(sys.argv) < 4 or not isInt(sys.argv[2]) or not isInt(sys.argv[3]):
    print('please specify input file, min length and max length')
    exit(1)

trees = []
curTree = ''
for line in open(sys.argv[1]):
    if len(line) < 3:
        if curTree != '':
            trees.append(curTree)
        curTree = ''
    else:
        if line[0] == '#':
            continue
        else:
            tok = line.split('\t')
            curTree += '\t'.join(tok[0:2] + [tok[4]] + tok[6:8]) + '\n'

minLen = int(sys.argv[2])
maxLen = int(sys.argv[3])
for tree in trees:
    treeLen = len(tree.split('\n'))
    if treeLen > minLen and treeLen < maxLen:
        print(tree)
