import sys


def isInt(s):
    try:
        int(s)
        return True
    except ValueError:
        return False

if len(sys.argv) < 2:
    print('please specify input file')
    exit(1)

totalWords = 0
totalSents = 0
totalDist = 0
curTree = []
for line in open(sys.argv[1]):
    if len(line) < 3:
        treeDist = 0
        totalSents += 1
        for wordIdx in range(len(curTree)):
            totalWords += 1
            headIdx = curTree[wordIdx]
            if headIdx == 0:
                treeDist += 0
            else:
                treeDist += abs(headIdx-(wordIdx + 1))
        if len(curTree) == 1:
            print(0.0)
        else:
            print((1/(len(curTree)-1)) * treeDist)
            totalDist += treeDist
        curTree = []
    else:
        if line[0] == '#':
            continue
        else:
            tok = line.strip().split('\t')
            if isInt(tok[0]):
                curTree.append(int(tok[6]))

print('Treebank: ' + str((1/(totalWords-totalSents)) * totalDist))
