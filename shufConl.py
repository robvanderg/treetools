import sys
import random
random.seed(8446)

trees = []
curTree = ''
for filePath in sys.argv[1:]:
    for line in open(filePath):
        if len(line) < 3:
            if curTree != '':
                trees.append(curTree)
            curTree = ''
        else:
            curTree += line

random.shuffle(trees)
for tree in trees: 
    print(tree)
