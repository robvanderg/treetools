# README #

C++ program to convert raw trees in ptb format to conll dependency format including gold pos tags

### INPUT ###
```
#!bash
(ROOT (SBARQ (WHNP (WP What) (DT a) (JJ nice) (NN application)) (. !)))
```
### OUTPUT ###
```
#!bash
What	WP	-	
a	DT	-	
nice	JJ	-	
application	NN	-	
!	.	-	
```

### Installation ###

Simply run icmbuild from this projects directory

```
#!bash
cd tree2conll
icmbuild
```

### Use ###


```
#!bash
./tmp/bin/binary input output
```
