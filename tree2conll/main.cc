#include "convert/convert.h"

#include <string>
#include <iostream>

int main(int argc, char *argv[])
{
    if (argc <3 )
    {
        std::cout << "usage: " + std::string(argv[0]) + " <treebankPath> <outputFile.conll>\n";
        exit(0);
    }
    Convert convert;
    convert.readBracket(std::string(argv[1]));
    convert.write(std::string(argv[2]));
}
