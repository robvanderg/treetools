#ifndef INCLUDED_CONVERT_
#define INCLUDED_CONVERT_

#include "../depthitem/depthitem.h"

#include <string>
#include <vector>

class Convert
{
    std::vector<std::vector<DepthItem>> d_trees;
    
    public:
        Convert();

        void readBracket(std::string treebankPath);
        void write(std::string conllPath);

    private:
};
        
#endif
