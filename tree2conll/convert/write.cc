#include "convert.ih"

void Convert::write(std::string conllPath)
{
    ofstream os(conllPath);
    std::string whiteSpace = "\t";
    for (size_t treeIdx = 0; treeIdx != d_trees.size(); ++treeIdx)
    {
        size_t wordIdx = 1;
        for (size_t nodeIdx = 0; nodeIdx != d_trees[treeIdx].size(); ++ nodeIdx)
        {
            if (d_trees[treeIdx][nodeIdx+1].depth -1 != d_trees[treeIdx][nodeIdx].depth)
                os << 
                      d_trees[treeIdx][nodeIdx].content << whiteSpace << 
                      d_trees[treeIdx][nodeIdx-1].content << whiteSpace << 
                      "-" << whiteSpace << '\n'; 
        }
        os << '\n';
    }
    os.close();
}
