#ifndef INCLUDED_DEPTHITEM_
#define INCLUDED_DEPTHITEM_

#include <string>

class DepthItem
{
    public:
        int depth;
        std::string content;

        DepthItem();
        DepthItem(int depth2, std::string content2);
        void print();
    private:
};
        
#endif
