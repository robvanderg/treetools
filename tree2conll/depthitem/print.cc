#include "depthitem.ih"

void DepthItem::print()
{
    int tabs = depth;
    while(tabs-- > 0){
        cout << "  ";
    }
    cout << content << '\n';
}
