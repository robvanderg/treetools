#include "convert.ih"

void Convert::print()
{
    for (size_t treeIdx = 0; treeIdx != d_trees.size(); ++treeIdx)
    {
        for (size_t nodeIdx = 0; nodeIdx != d_trees[treeIdx].size(); ++nodeIdx)
        {
            d_trees[treeIdx][nodeIdx].print();
        }
    }
}
