#include "convert.ih"

void Convert::writeLatex(string latexPath, size_t treeNum)
{
    ofstream os(latexPath);
    os << "\\begin{tikzpicture}[scale=0.6]\n" 
       << "\\Tree\n";
    for (size_t nodeIdx = 0; nodeIdx != d_trees[treeNum].size(); ++nodeIdx)
    {
        DepthItem curNode = d_trees[treeNum][nodeIdx];
        int nextDepth = 0;
        int curDepth = curNode.d_depth;
        if (nodeIdx != d_trees[treeNum].size()-1)
            nextDepth = d_trees[treeNum][nodeIdx+1].d_depth;

        for (size_t indent = curDepth; indent != 0; --indent)
            os << "  ";
        os << '[';

        // if non-terminal
        if (curDepth + 1 == nextDepth)
            os << '.';

        // escape latex special characters
        string content = curNode.d_content;
        replaceAll(content, "\\", "\\textbackslash{}");
        replaceAll(content, "^", "\\textasciicircum{}");
        replaceAll(content, "~", "\\textasciitilde{}");
        for (char c: "#$%&_{}")
            replaceAll(content, string(1,c), "\\" + string(1,c));
        // . only needs to be escaped as a terminal
        if (content == "." && curDepth + 1 != nextDepth)
            os << "{.}";
        else
            os << content;

        // if terminal
        if (curDepth + 1 != nextDepth)
            os << " ]";
        os << '\n';

        // closing brackets
        for(int closing = curDepth; closing > nextDepth; --closing)
        {
            for(int spaces = closing-1; spaces != 0; --spaces)
                os << "  ";
        os << "]\n";
        }
        
    }
    os << "\\end{tikzpicture}\n";
    os.close();
}

