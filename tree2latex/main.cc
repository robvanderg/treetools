#include "convert/convert.h"

#include <iostream>
#include <string>
#include <fstream>

int main(int argc, char *argv[])
{
    if (argc < 3)
    {
        std::cout << "where are my arguments? <input trees> <output name>\n";
        exit(0);
    }
    Convert conv;
    conv.readBracket(argv[1]);
    for (size_t treeNum = 0; treeNum != conv.numTrees(); ++treeNum)
        conv.writeLatex(std::string(argv[2]) + std::to_string(treeNum + 1) + ".tex", treeNum);

    std::ofstream os(std::string(argv[2]) + ".tex");
    os << "\\documentclass{article}\n\n" 
       << "\\usepackage{tikz-qtree}\n\n"
       << "\\begin{document}\n\n";
    for (size_t treeNum = 0; treeNum != conv.numTrees(); ++treeNum)
    {
        os << "\\input{" << argv[2] << treeNum+1 << "}\\\\ \n";
        if ((treeNum +1) %3 == 0 && treeNum != conv.numTrees()-1)
            os << "\\clearpage\n";
    }
    os << "\n\\end{document}\n";
    os.close();    
}
