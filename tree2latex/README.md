# README #

A simple c++ application to convert PTB style trees to latex's tikz-qtree format (nicely indented)

### INPUT ###
```
#!bash
(ROOT (SBARQ (WHNP (WP What) (DT a) (JJ nice) (NN application)) (. !)))
```
### OUTPUT ###
```
#!bash
\begin{tikzpicture}[scale=0.6]
\Tree
[.ROOT
  [.SBARQ
    [.WHNP
      [.WP
        [What ]
      ]
      [.DT
        [a ]
      ]
      [.JJ
        [nice ]
      ]
      [.NN
        [application ]
      ]
    ]
    [..
      [! ]
    ]
  ]
]
\end{tikzpicture}
```


### Installation ###

Simply run icmbuild from this projects directory

```
#!bash
cd tree2latex
icmbuild
```

### Use ###


```
#!bash
./tmp/bin/binary input output
pdflatex output
```