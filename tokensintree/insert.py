import sys

if len(sys.argv) < 3:
    print("I need data! <trees> <ne>")
    exit()

trees = open(sys.argv[1], 'r')
nes = open(sys.argv[2], 'r')

for ne in nes:
    tree = trees.readline()[:-1]
    start = 0
    for word in ne.split():
        word = word[:word.rfind('/')] #chunking
        word = word[:word.rfind('/')] #pos tagging
        neTag = word[word.rfind('/')+1:] 
        word = word[:word.rfind('/')]
        position = tree.find(word, start)
        word = word.replace('(', "-LRB-")
        word = word.replace(')', "-RRB-")
        
        #print(tree, '\n', '-'+word+'-', tree[position + len(word)], tree[position-1])
        while tree[position + len(word)] != ')' or tree[position-1] != ' ':
            position = tree.find(word, position+1)
        start = position
        if neTag != 'O' and word != "-LRB-" and word != "-RRB-":
            bef = tree[:position]
            tree = tree[position:]
            tree = tree[tree.find(')'):]
            tree = bef + neTag + tree
    print (tree)

            
        


