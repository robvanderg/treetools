import sys

if len(sys.argv) < 3:
    print('please give inputFile start end')
    exit(1)

trees = []
curTree = ''
for line in open(sys.argv[1]):
    if len(line) < 3:
        if curTree != '':
            trees.append(curTree)
        curTree = ''
    else:
        curTree += line

for tree in trees[int(sys.argv[2]):int(sys.argv[3])]:
    print(tree)
