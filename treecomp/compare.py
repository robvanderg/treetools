import sys

if len(sys.argv) < 6:
    print('usage', sys.argv[0] + ' <res1(evalb)> <res2(evalb)> <out1> <out2> <gold>')
    exit()

eval_van = open(sys.argv[1], 'r')
eval_norm =  open(sys.argv[2], 'r')

for i in range(3):
    eval_van.readline()
    eval_norm.readline()

results = {}

while True:
    van = eval_van.readline()
    norm = eval_norm.readline()
    if van[0] == '=':
        break
    v_prec = float(van.split()[3])
    v_rec = float(van.split()[4])
    v_f = 0.0
    if (v_prec + v_rec) != 0.0:
        v_f = 2 * (v_prec * v_rec)/ (v_prec + v_rec)

    n_prec = float(norm.split()[3])
    n_rec = float(norm.split()[4])
    n_f = 0.0
    if (n_prec + n_rec) != 0.0:
        n_f = 2 * (n_prec * n_rec)/ (n_prec + n_rec)
    diff = v_f - n_f
    if (diff != 0.0):
        results[van.split()[0]] = diff

tenBest = []
for item in sorted(results.items(), key=lambda x: x[1])[:10]:
    tenBest.append(item[0])

tenWorst = []
for item in sorted(results.items(), key=lambda x: x[1])[-10:]:
    tenWorst.append(item[0])


#saldo = 0.0
#for item in sorted(results.items(), key=lambda x: x[1]):
#    saldo += item[1]
#    #print(item)
#print(saldo)



vanF = open(sys.argv[3], 'r')
normF = open(sys.argv[4], 'r')
goldF = open(sys.argv[5], 'r')

def repl(word):
    return word#.replace('(','[').replace(')', ']')

print('Best:')
counter = 1
for goldTree in goldF:
    normTree = repl(normF.readline())
    vanTree = repl(vanF.readline())
    if str(counter) in tenBest:
        print(repl(goldTree), vanTree, normTree)
    counter += 1

vanF = open(sys.argv[3], 'r')
normF = open(sys.argv[4], 'r')
goldF = open(sys.argv[5], 'r')
print('Worst:')
counter = 1
for goldTree in goldF:
    normTree = repl(normF.readline())
    vanTree = repl(vanF.readline())
    if str(counter) in tenWorst:
        print(repl(goldTree), vanTree, normTree)
    counter += 1

