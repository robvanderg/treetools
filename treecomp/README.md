# README #

Python script to find the major differences in performace between 2 parser (simple version of the Berkeley Parser Analyzer: https://github.com/jkkummerfeld/berkeley-parser-analyser)

(Nice to use with tree2latex)

### INPUT ###
```
evalb file of 1st parser
evalb file of 2nd parser
trees of 1st parser
trees of 2nd parser
gold trees
```
### OUTPUT ###
```
list of 10 best parsers of 1st parser:
    gold tree
    1st parser result
    2nd parser result

list of 10 best parsers of 2nd parser
    gold tree
    1st parser result
    2nd parser result
```

### Use ###

Simply run python3 compare.py <args> from this projects directory

