import sys


def isInt(s):
    try:
        int(s)
        return True
    except ValueError:
        return False
if len(sys.argv) < 2:# or not isInt(sys.argv[2]) or not isInt(sys.argv[3]):
    print('please specify input file, min length and max length')
    exit(1)

trees = []
curTree = []
for line in open(sys.argv[1]):
    if len(line) < 3:
        if curTree != '':
            trees.append(curTree)
        curTree = []
    else:
        tok = line.split('\t')
        if line[0] == '#' or not isInt(tok[0]):
            continue
        else:
            curTree += [tok[0:2] + [tok[3]] + tok[6:8]]

minLen = int(sys.argv[2])
maxLen = int(sys.argv[3])
for tree in trees:
    proj = True
    hint = ''
    for curPos in range(len((tree))):
        # have dependent, now get head
        head = int(tree[curPos][-2]) -1
        start = min(curPos, head) 
        end = max(curPos, head)  
        for cand in range(start+1,end):
            candHead = int(tree[cand][-2]) -1
            if candHead == -1:
                continue
            if (candHead < start or candHead > end):# and tree[candHead][3] != '0' and tree[curPos][3] != '0':
                proj = False
                hint = 'word at pos ' + str(cand + 1) + ' goes out of span ' + str(start + 1) + '-' + str(end+1)

    if not proj and len(tree) < maxLen and len(tree) > minLen:
        print(hint)
        for node in tree:
            print('\t'.join(node))
        print()
