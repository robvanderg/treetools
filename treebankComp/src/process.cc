#include "main.ih"

void process(int argc, char*argv[])
{
    vector<string> treeBanks;
    Config mainConfig;
    for(int argIdx = 1; argIdx < argc; ++argIdx){
        switch(getAction(argv[argIdx]))
        {
            case Argument::VERTICAL:
                
                break;
            case Argument::POSINFO:
    
                break;
            case Argument::TOKENINFO:
    
                break;
            case Argument::LABELINFO:
    
                break;
            case Argument::TREELOC:
                treeBanks.push_back(argv[argIdx]);
                break;
        }
    }
    
    vector<Stats> results;
    for(string treeBankPath: treeBanks){
        GrammarReader reader(treeBankPath);
        results.push_back(reader.stats());
    }
    Printer myPrinter(mainConfig);
    myPrinter.printAll(results);
}
