#include "stats.ih"

void Stats::countText(vector<string> tokens)
{
    ++d_sentCount;

    d_wordCount += tokens.size();

    for (string token: tokens){

        d_charCount += token.length();

        if (d_dict.count(token)!=0)
            ++d_ivCount;
        else
            ++d_oovCount;

        d_tokens.insert(token);

    }

}
