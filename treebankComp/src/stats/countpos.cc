#include "stats.ih"

void Stats::countPos(vector<string> posTags)
{
    for(string posTag: posTags){
        if (posCounts.count(posTag)>0)
            posCounts[posTag] = posCounts[posTag] + 1;
        else
            posCounts[posTag] = 1;
    }
}
