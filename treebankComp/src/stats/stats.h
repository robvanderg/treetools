#ifndef INCLUDED_STATS_
#define INCLUDED_STATS_

#include "../config/config.h"
#include "../depthitem/depthitem.h"

#include <vector>
#include <string>
#include <set>
#include <unordered_map>

class Stats
{
    std::string d_name;
    size_t d_sentCount, d_wordCount, d_charCount, d_ivCount, d_oovCount; 
    std::set<std::string> d_dict;
    std::set<std::string> d_tokens;

    std::unordered_map<std::string,size_t> posCounts;

    public:
        Stats();
        void setName(std::string name); 
        void add(std::vector<DepthItem> depthList);
        
        std::vector<std::string> toVector(Config mainConfig);

    private:
        std::string floatToString(float value, size_t prec);
        
        void countText(std::vector<std::string> tags);
        void countPos(std::vector<std::string> tokens);
};
        
#endif
