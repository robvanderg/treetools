#include "stats.ih"

vector<string> Stats::toVector(Config mainConfig)
{
    vector<string> stats;
    stats.push_back(d_name);
    stats.push_back(to_string(d_sentCount));
    stats.push_back(floatToString( (float)d_wordCount / d_sentCount , mainConfig.d_prec));
    stats.push_back(floatToString( (float)d_charCount / d_wordCount , mainConfig.d_prec));
    stats.push_back(floatToString( (float)d_oovCount / (d_oovCount + d_ivCount), mainConfig.d_prec));
    stats.push_back(to_string(d_tokens.size()));
    stats.push_back(to_string(posCounts.size()));
    return stats;
}
