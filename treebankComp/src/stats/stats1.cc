#include "stats.ih"

Stats::Stats()
//:
{
    d_sentCount = 0;
    d_wordCount = 0;
    d_charCount = 0;
    d_ivCount = 0;
    d_oovCount = 0;

    std::ifstream ifs("dict");
    string token = "";
    while(ifs >> token)
        d_dict.insert(token);
}
