#include "stats.ih"

void Stats::add(vector<DepthItem> depthList)
{
    vector<vector<string>> rules;
    vector<string> tokens;
    vector<string> pos;
    for (size_t leftItr = 0; leftItr < depthList.size(); ++leftItr){
        vector<string> rule;
        rule.push_back(depthList[leftItr].d_tag);
        for (size_t rightItr = leftItr + 1; rightItr < depthList.size(); ++rightItr){
            if(depthList[rightItr].d_depth == depthList[leftItr].d_depth){
                break;
            }
            if (depthList[leftItr].d_depth +1 == depthList[rightItr].d_depth){
                rule.push_back(depthList[rightItr].d_tag);
            }
        }
        if (rule.size() == 1){
            tokens.push_back(rule[0]);
            pos.push_back(rules.back()[0]);
            rules.pop_back();
        }
        else
            rules.push_back(rule);
    }
    countText(tokens);
    countPos(pos);
}
