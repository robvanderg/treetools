#include "stats.ih"

string Stats::floatToString(float value, size_t prec)
{
    stringstream stream;
    stream << fixed << setprecision(prec) << value;
    return stream.str();
}
