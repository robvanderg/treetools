#include "main.ih"

void usage(std::string const &progName)
{
    cout << progName <<
    "\n\n" <<
    "Usage: " << progName << " path_treebank1 ... path_treebankN" <<
    "\n";
}
