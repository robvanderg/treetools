#ifndef INCLUDED_GRAMMARREADER_
#define INCLUDED_GRAMMARREADER_

#include <string>
#include <vector>

#include "../stats/stats.h"
#include "../depthitem/depthitem.h"

class GrammarReader
{
    Stats d_stats;

    std::string d_path;

    public:
        GrammarReader(std::string path);
        void walk(std::string path);
        void deleteOtherFiles();

        Stats stats();

    private:
        void readFile(std::string path);
        void readBracket(std::string parsePath);
};

#endif
