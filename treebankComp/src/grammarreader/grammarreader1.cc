#include "grammarreader.ih"

GrammarReader::GrammarReader(string path)
//:
{
    d_path = path;
    if (path.back() == '/') 
        path = path.substr(0,path.length()-1);
    d_stats.setName(path.substr(path.find_last_of('/') + 1, path.length()));
    walk(path);

}
