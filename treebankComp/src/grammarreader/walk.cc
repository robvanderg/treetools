#include "grammarreader.ih"

void GrammarReader::walk(string dirname){
    DIR *tem=opendir(dirname.c_str());
    struct dirent *direntp;
    struct stat buf;
    char t[256];
    
    if(tem==NULL)
        cout << "stderr ls: cannot open " << dirname << "\n";
    else{
        while((direntp=readdir(tem))!=NULL){
            if (direntp->d_name[0] != '.'){ // skip paths that start with a '.'
                strcpy(t,dirname.c_str());
                strcat(t,"/");
                strcat(t,direntp->d_name);
                if(stat(t,&buf) < 0)
                    break;
                else
                    if((buf.st_mode & S_IFMT) == S_IFDIR)
                        walk(t); //this is a dir, find the files inside the dir.
                    else
                        readFile(string(t));
            }
        }
        closedir(tem);
    }
}
