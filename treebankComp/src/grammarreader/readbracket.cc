#include "grammarreader.ih"

void GrammarReader::readBracket(string parsePath)
{
    vector<DepthItem> depthList;
    int depth = 0;
    string tag = "";

    ifstream is(parsePath);
    char c;
    while (is.get(c)){
        switch (c){
            case '(':
                ++depth;
                break;
            case ')':
                if(tag.length() > 0){
                    depthList.push_back(DepthItem(++depth, tag));
                    tag = "";
                    --depth;
                }
                --depth;
                if(depth == 0){
                    d_stats.add(depthList);
                    depthList.clear(); 
                    tag="";
                }
                break;
            case ' ':
                if(tag.length() > 0){
                    depthList.push_back(DepthItem(depth, tag));
                    tag = "";
                }
                break;
            case '\n':
                break;
            default:
                tag += c;
                break;
        }
    }
}

