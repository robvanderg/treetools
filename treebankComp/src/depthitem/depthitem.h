#ifndef INCLUDED_DEPTHITEM_
#define INCLUDED_DEPTHITEM_

#include <string>
#include <vector>

class DepthItem
{
    public:
        size_t d_depth;
        std::string d_tag;

        DepthItem(size_t depth, std::string tag);
        void print();

    private:
};
        
#endif
