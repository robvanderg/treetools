#include "depthitem.ih"

void DepthItem::print()
{
    size_t tabs = d_depth;
    while(tabs-- > 0){
        cout << "  ";
    }
    cout << d_tag << '\n';
}
