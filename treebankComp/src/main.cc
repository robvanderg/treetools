#include "main.ih"

int main(int argc, char* argv[]){
    if (argc == 1) // geen argumenten meegegeven
        usage(argv[0]);
    else
        process(argc, argv);
}
