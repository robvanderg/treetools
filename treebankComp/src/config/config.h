#ifndef INCLUDED_CONFIG_
#define INCLUDED_CONFIG_

#include <string>
#include <vector>

class Config
{
    public: // all vars are global
        size_t d_prec = 2;
        
        bool d_horizontal = true;
//        const bool d_posInfo;
//        const bool d_tokenInfo;
//        const bool d_labelInfo;



        static const size_t d_featSize = 7;
        const std::string d_features[d_featSize] = 
            {"Name", 
                "Sents", 
                "Word/S", 
                "Char/W", 
                "\%OOV", 
                "Voc", 
                "#POS"};

        Config(bool horizontal = true, size_t prec = 2);

    private:
};
        
#endif
