#include "printer.ih"

void Printer::printHorizontal(string allResults[][Config::d_featSize], size_t numBanks)
{
    size_t widths[Config::d_featSize];
    for (size_t featIdx = 0; featIdx < Config::d_featSize; ++featIdx){
        widths[featIdx] = 0;
        for(size_t bankIdx = 0; bankIdx < numBanks; ++bankIdx){
            if (allResults[bankIdx][featIdx].length() > widths[featIdx]){
                widths[featIdx] = allResults[bankIdx][featIdx].length();
            }
        }
    }
    
    for (size_t bankIdx = 0; bankIdx < numBanks; ++bankIdx){
        for(size_t featIdx = 0; featIdx < Config::d_featSize; ++featIdx){
            cout << setw(widths[featIdx]+1) << allResults[bankIdx][featIdx] << ' ';
        }
        cout << '\n';
    }
}
