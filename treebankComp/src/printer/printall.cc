#include "printer.ih"

void Printer::printAll(vector<Stats> results)
{
    string all_data[results.size() + 1][Config::d_featSize];
    for (size_t colIdx = 0; colIdx < Config::d_featSize; ++colIdx){
        all_data[0][colIdx] = d_config.d_features[colIdx];
    }

    for (size_t bankIdx = 0; bankIdx < results.size(); ++bankIdx)
    {
        vector<string> counts = results[bankIdx].toVector(d_config);
        for (size_t featIdx = 0; featIdx < Config::d_featSize; ++featIdx){
            all_data[bankIdx + 1][featIdx] = counts[featIdx];
        }
    }
    if (d_config.d_horizontal)
        printHorizontal(all_data, results.size() + 1);
//    else
//        printVertical(all_data, results.size() + 1);
}
