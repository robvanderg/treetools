#ifndef INCLUDED_PRINTER_
#define INCLUDED_PRINTER_

#include "../config/config.h"
#include "../stats/stats.h"

#include <vector> 

class Printer
{
    Config d_config;

    public:
        Printer(Config mainConfig);
        void printAll(std::vector<Stats> results);
    private:
        void printHorizontal(std::string allResults[][Config::d_featSize], size_t y);
        void printVertical(std::string allResults[][Config::d_featSize], size_t x);
};
        
#endif
