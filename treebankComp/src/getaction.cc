#include "main.ih"

Argument getAction(string const &arg)
{
    if (arg == "-v")
        return Argument::VERTICAL;
    if (arg == "-t")
        return Argument::TOKENINFO;
    if (arg == "-p")
        return Argument::POSINFO;
    if (arg == "-l")
        return Argument::LABELINFO;

    return Argument::TREELOC;
}
