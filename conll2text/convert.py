import sys

if len(sys.argv) < 2:
    print("where is my inputfile?")
    exit()

for line in open(sys.argv[1]):
    if len(line) > 2:
        print(line.split()[1], end=' ')
    else:
        print()
